<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PassportAuthController;
use App\Http\Controllers\Api\LoanController;
use App\Http\Controllers\Api\LoanDetailController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);

// put all api protected routes here
Route::middleware('auth:api')->group(function () {
    Route::post('user-detail', [PassportAuthController::class, 'userDetail']);
    Route::post('logout', [PassportAuthController::class, 'logout']);

// LoanController all routes for application routes
    Route::patch('loanApprove/{id}', [LoanController::class, 'update'])->name('loanApprove')->middleware("CheckAdmin");
    Route::post('addLoan', [LoanController::class, 'store'])->name('addLoan');
    Route::get('getLoanDetails/{id}', [LoanController::class, 'show'])->name('getLoanDetails');
    Route::get('GetLoanData', [LoanController::class, 'index'])->name('GetLoanData');



// LoanDetailController all routes for application routes
    Route::put('paid_partial_loan', [LoanController::class, 'paid_partial_loan'])->name('paid_partial_loan');
});
