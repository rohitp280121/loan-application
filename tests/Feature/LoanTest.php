<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Loan;
use App\LoanDetail;
use App\User;

class LoanTest extends TestCase
{
    public function testRequiredFieldsForLoan()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $this->json('POST', 'api/addLoan', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "Please see errors parameter for all errors.",
                "errors" => [
                    "term" => ["The term field is required."],
                    "amount" => ["The amount field is required."],
                    "user_id" => ["The user id field is required."],
                ]
            ]);
    }


    public function testSuccessfulAddLoan()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $userData = [
            "term" => 2,
            "amount" => 100,
            "user_id" => 2,
        ];

        $this->json('POST', 'api/addLoan', $userData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "message"
            ]);
    }

    public function testGetLoanDetails()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');


        $this->json('GET', 'api/GetLoanData', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "message",
                "result"=> []
            ]);
    }

}
