<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = [
        'total_paid_term',
        'amount',
        'term',
        'paid_status',
        'user_id'
    ];
}
