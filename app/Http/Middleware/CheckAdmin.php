<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->user_type != 'Admin') {
            return response()->json([
                'success' => false,
                'message' => 'You does not have permission to access this Api.'
            ], 401);
        }


        return $next($request);
    }
}
