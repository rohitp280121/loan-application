<?php



namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Loan;
use App\LoanDetail;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class LoanController extends Controller
{

    private $loan;
    private $loanDetail;
    public function  __construct(Loan $loan, LoanDetail $loanDetail)
    {
        $this->loan = $loan;
        $this->loanDetail = $loanDetail;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $loanData = $this->loan->where('user_id', auth()->user()->id)->get();

        return response()->json([
            'success' => true,
            'message' => 'User Loan Date Retrieved Successfully.',
            'result'=>$loanData
        ], 200);
    }

    /**
     * Show the form for creating a new resource.

     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * This function is used to create a new Loan
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->only(['term', 'amount', 'user_id']);

        $validate_data = [
            'term' => 'required|numeric|min:1',
            'amount' => 'required|numeric',
            'user_id' => 'required|numeric|exists:users,id',
        ];
        $validator = Validator::make($input, $validate_data);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Please see errors parameter for all errors.',
                'errors' => $validator->errors()
            ], 422);
        }


        $loan = $this->loan->create([
            'term' => $request->term,
            'amount' => $request->amount,
            'user_id' => $request->user_id,
            'total_paid_term' => 0,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'User Requested Loan details Inserted successfully.'
        ], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $loanData = $this->loan->where('id', $id)->first();
        if (is_null($loanData)) {
            return response()->json([
                'success' => false,
                'message' => 'Loan Details Not Found.',
                'errors' => 'Loan Details Not Found.'

            ], 200);
        } else {
            if ($loanData['paid_status'] != 'Approved') {
                $this->loan->where('id', $id)
                    ->update([
                        'paid_status' => 'Approved'
                    ]);

                $loanDetails = array();

                for ($i = 0; $i < $loanData['term']; $i++) {
                    $daycount = 10 * ($i + 1);
                    $newDateTime = Carbon::now()->addDays($daycount);
                    $loanDetails[$i]['loan_id'] =  $id;
                    $loanDetails[$i]['amount'] =  round($loanData['amount'] / $loanData['term'], 2);
                    $loanDetails[$i]['loan_paying_date'] = $newDateTime->toDateString();
                }
                $this->loanDetail->insert($loanDetails);

                return response()->json([
                    'success' => true,
                    'message' => 'User Loan Requested Approved successfully.'
                ], 200);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => 'User Loan Already Approved.'
                ], 200);
            }
        }
    }


    public function paid_partial_loan(Request $request)
    {

        $input = $request->only(['id', 'amount']);

        $validate_data = [
            'amount' => 'required|numeric',
            'id' => 'required|numeric|exists:loan_details,id',
        ];
        $validator = Validator::make($input, $validate_data);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Please see errors parameter for all errors.',
                'errors' => $validator->errors()
            ]);
        } else {
            $loanDetails = $this->loanDetail->where('id', $request->id)->first();
            $loanData = $this->loan->where('id', $loanDetails['loan_id'])->first();

            if ($loanDetails) {
                if ($loanDetails['paid_status'] == 'Unpaid') {
                    if ($request->amount >= $loanDetails['amount']) {
                        $this->loanDetail->where('id', $request->id)
                            ->update([
                                'paid_status' => 'Paid'
                            ]);
                        $count = $this->loanDetail
                            ->where('loan_id', $loanDetails['loan_id'])
                            ->where('paid_status', 'Paid')
                            ->count();

                        $this->loan->where('id',$loanDetails['loan_id'])
                            ->update([
                                'total_paid_term' => $count,
                                'paid_status' => $count==$loanData['total_paid_term'] ? 'Paid' : 'Approved',

                            ]);

                        return response()->json([
                            'success' => true,
                            'message' => 'Loan Amount Paid successfully.'
                        ], 200);
                    } else {
                        return response()->json([
                            'success' => false,
                            'message' => 'Amount should be grater than or equal to paid amount.',
                            'errors' => $validator->errors()
                        ]);
                    }
                } else {
                    return response()->json([
                        'success' => true,
                        'message' => 'You Have already Paid this Amount.'
                    ], 200);
                }
            }
        }
    }
}
