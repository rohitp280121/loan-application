<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanDetail extends Model
{
    protected $fillable = [
        'loan_id',
        'amount',
        'paid_status',
        'loan_paying_date'
    ];
}
