# Steps You Need to follow for Project Setup

1) git clone https://gitlab.com/rohitp280121/loan-application.git


In this step we are create new .env file from example.env
we can use .env file for database connection and email configuration setting etc.

2)
i) cd loan-application
ii) cp .env.example .env

In this step we are install the application laravel packages
3) composer install

In this step we are genrated a application key for project.
4)php artisan key:generate

Here we Use laravel passport package to create a new authenticate token for every login User.
this package is most of use for authenticate tokens and using this package we need pass authentication token for access api which is under auth middleware.

5)composer require laravel/passport

Here we migrate all our migration to our database 

6)
i)php artisan migrate
ii)php artisan passport:install

Here we seed Admin details to our user table 
7)php artisan db:seed

For Unit Test You Only Need To below Command
8)composer test

Postman collection public link 
Link:https://www.getpostman.com/collections/4b7bbcd61bb8a7c4d49b
by using this link you can import all apis on your postman tool by import method 

That's it Thanks 
